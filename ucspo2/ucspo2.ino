#include <ArduinoJson.h>

//-------------------pulse oximetry--------------------
#include "XMV20_SpO2v2.h"
//RESETpin, SPIEN, AFE_PDN, ADC_RDY,
//PB3/43, PB2/42, PD2/11, PB4/44,
//3,2,10,4/
SpO2_AFE44x0  mySpO2(3,2,10,24,SPISettings(2e6, MSBFIRST, SPI_MODE0));
#include <avr/wdt.h>
uint8_t valid;
int lastvalue;
int avg;
// #define PIN_PB4 PINB4
//-------------------pulse oximetry--------------------

//---------------------Temperature---------------------
#include <OneWire.h>
#include <DallasTemperature.h>
//30/PA7 -->31
#define ONE_WIRE_BUS 31
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature BDTemp(&oneWire);
DeviceAddress deviceAddress;
int16_t userdata;
float calibration_offset;
float temperature;

//others
uint8_t x;
//----------------------time var-----------------------
#define PERIOD 20UL
// #define PERIOD2 1000UL
void setup(void){
 
    // digitalWrite(PIN_PB4,HIGH);
    // pinMode(PIN_PB4, OUTPUT);
    BDTemp.begin();
    BDTemp.getAddress(deviceAddress,0);
	BDTemp.setAutoSaveScratchPad(false);
	userdata=BDTemp.getUserData(deviceAddress);
	// BDTemp.setUserData(deviceAddress,-15);
	// BDTemp.saveScratchPad(deviceAddress);
	if(userdata<=10000) calibration_offset=userdata*0.01;
    Serial.begin(57600);
    Serial1.begin(38400);
    Serial.println("Test");
    printValues();
	  
    mySpO2.initIO();
    delay(1000);
    mySpO2.initSPI();
    delay(1000);
}

    int32_t spO2;
	int BPM;

  bool temp_get=false;
void loop(void){
  if( mySpO2.refresh() )wdt_reset();
  static unsigned long lastTime = millis();
  unsigned long currentTime = millis();
  if((currentTime - lastTime) > PERIOD){
    lastTime += PERIOD;
	
    BPM=round(mySpO2.getBPM());
	spO2 = round(mySpO2.getSpO2());
	if(BDTemp.isConversionComplete() && !temp_get){
	  temperature=BDTemp.getTempCByIndex(0);
	  temp_get=true;
	}
    //---------------------------valid---------------------------
    if(spO2>=99)   spO2=99;
    if(spO2 >= 0 ) avg=moving_average(spO2);
    switch(valid){
      case 0:{if(BPM!=0) valid=1; }break;
      case 1:{if(spO2<0) valid=2; else if(BPM==0) valid=3;}break;
      case 2:{if(BPM==0) valid=0; }break;
      case 3:{if(BPM!=0) valid=1; else if(spO2<0) valid=2;}break;
    }
    switch(valid){
      case 0:{spO2 = -1;}break;
      case 1:{spO2=avg;lastvalue=spO2;}break;
      case 2:{spO2 = -1;}break;
      case 3:{spO2=lastvalue;}break;
    } 
        // Serial.print(mySpO2.getNrmIR());
        // Serial.print(' ');
        // Serial.print(mySpO2.getNrmIR());
      
        // Serial.println();
    //---------------------------valid---------------------------  
  }
  static unsigned long lastTime2 = millis();
  if((currentTime - lastTime2) > 1000){
    lastTime2 += 1000;
	//---------------------------tempB---------------------------
		Serial.print(temp_get);
		Serial.println(" temp_get");
	if(temp_get){
	  temp_get=false;
	  BDTemp.setWaitForConversion(false);
      BDTemp.requestTemperatures();
    }
    //---------------------------tempB---------------------------

    //---------------------------comm---------------------------
	temperature+=calibration_offset;
    StaticJsonDocument<150> monitor_transmit;
    char temp_transmit[150];
    monitor_transmit["V"]=valid;
    monitor_transmit["S"]=spO2;
	monitor_transmit["T"]=round(temperature*100);
    serializeJson(monitor_transmit, temp_transmit);
    Serial1.println(temp_transmit);
    //---------------------------comm---------------------------

    if(spO2 >= 0 ) Serial.print(spO2);
    else           Serial.print("N/A");
        Serial.print(' ');
        Serial.print(BPM);
        Serial.print(' ');
        Serial.print(valid);
        Serial.print(' ');
        Serial.print(temperature);
        Serial.print(' ');
		Serial.println();
  } 
}
int moving_average(int in){
  static int readIndex=0;
  static long total;
  static int sample[5];

  total = total - sample[readIndex];
  sample[readIndex] = in;
  total = total + sample[readIndex];
  readIndex = readIndex + 1;

  if(readIndex>=5){
	readIndex=0;
  }
  return round(total*0.2);

}
void printValues() {
  
  Serial.println();
  Serial.println("Current values on the scratchpad:");
  
  Serial.print("Resolution:\t");
  Serial.println(BDTemp.getResolution(deviceAddress));
  
  Serial.print("User data:\t");
  Serial.println(BDTemp.getUserData(deviceAddress));
  
}