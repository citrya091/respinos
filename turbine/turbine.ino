
#include <Wire.h>
#include <avr/wdt.h>
#include <SoftwareSerial.h>
#include <ArduinoJson.h>

bool overflow;
bool neg;
float flow;
double period;

char tipe;
union intToBytes {
    char buffer[2];
    int intreading;
  } iconverter;
union floatToBytes {
    char buffer[4];
    float flowReading;
  } fconverter;
  
SoftwareSerial mySerial(13, 12);
StaticJsonDocument<70> fromUC;
char msg[70];
int i;
int valid,spo2,BodyTemperature;
int last_spo2,last_BodyTemperature;

unsigned long now, lastDebug,lastgetflow;
unsigned long lasti2creq;

void setup(){
  Serial.begin(57600);
  mySerial.begin(38400);
  i=0;
  
  digitalWrite(5,HIGH);
  digitalWrite(14,HIGH);
  // TCCR1B=(1<<CS12)|(1<<CS11)|(1<<CS10);
  TCCR1A = 0;
  TCCR1B = 0;
  TCCR1B=(1<<CS12);
  PCICR|=(1<<PCIE2);
  PCMSK2=(1<<PCINT21);
  // TIMSK1|=(1<<TOIE1);
  // wdt_enable(WDTO_8S);
  Serial.println("start");
  Serial.println(TIMSK1);
  Wire.begin(8);
  Wire.onRequest(requestEvent);
}
void loop(){
  now=millis();
  if(now-lastgetflow >= 10){
    lastgetflow=now;
	if(period>42)
     flow=(neg)?((float)-42817/period):((float)42817/period);
	if(TCNT1>=62000){
	  flow=0; period=0;  
	}
  }
  getDataFromUC();
}

ISR(PCINT2_vect){
  if(!digitalRead(5)){
    neg=!digitalRead(14);
	period=TCNT1;
    Serial.println(flow);
    TCNT1=0;
  }
}

// ISR(TIMER2_OVF_vect){
// flow=0;period=0;
// }

void requestEvent(){
  fconverter.flowReading = flow;
  // PCICR=0;
  if(now - lasti2creq >=10){
    lasti2creq=now;
    Wire.write(char('f'));
    Wire.write(fconverter.buffer, 4);
  } else {
    Wire.write(char('d'));
	iconverter.intreading = spo2;
	Wire.write(iconverter.buffer,2);
	iconverter.intreading = BodyTemperature;
	Wire.write(iconverter.buffer,2);
  }
  // PCICR=(1<<PCIE2);
  // Serial.println(period);
}
void getDataFromUC() {
    if (mySerial.available() > 0) {
	  msg[i] =mySerial.read();
	  // Serial.print(msg[i]);
	  i++;
    }
	if( msg[i-1]=='\n'|| i>=40) {
	  parseUCmsg();
	  for(int x=0; x<40; x++){
        msg[x]=0;
	  }
	  i=0;
	}
}
void parseUCmsg(){
  Serial.print(i); Serial.print("\t");Serial.print(msg);
  deserializeJson(fromUC, msg);
  last_spo2=spo2;
  last_BodyTemperature=BodyTemperature;
  valid=fromUC["V"];
  spo2=fromUC["S"];
  BodyTemperature=fromUC["T"];
  
  if(spo2==-1)spo2=0;
  else if(valid==0||valid==2 || spo2<60) spo2=last_spo2;
  
  if(BodyTemperature<2000)
    BodyTemperature=last_BodyTemperature;
  else {
    if(BodyTemperature < 3200 || BodyTemperature > 4200)
      BodyTemperature=0;
  }

}
