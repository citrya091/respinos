


void setupRespirasiData();
float readFlowSensor();
void ProccessData(float readings);
void volumecalc();
void debug();

void IEDetection();
float filter2Hz();
void insertFlow(float readings);
void derivative2nd(float filter);
void peakDetectPos ();
void peakDetectNeg ();

void saveToArray();
void FVCManuverE();
void FVCManuverI();
void FVCinit();
void FVCreset();
void isFVCmanuver();
int getFVC();
int getFEV1();
float getratio();
float getPEF();
int getVTLength();
int getFVLength();
float getFlowArray(int idx);
float getVolumeArray(int idx);
void FVCresult();
void avgVOUT(int vout, bool reset);

void resetBreathCharValue();
void calcRR();
void guardRR();
void resetRR();
int getRRData();

float getFlowData();
int getVolumeRTData();
void setSensitivity(float sensitivity);
void setMonitorTimer(int seconds);
void setATemperature(int AT);
void loadAllSetting();
void setSTOP_FLAG ();
void setRUN_FLAG ();
void resetFIN_FLAG();
void setMODE_FLAG(int mode_en);
void TimerforMonitor();
