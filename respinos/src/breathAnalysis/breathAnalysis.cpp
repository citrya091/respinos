
#include "./breathAnalysis.h"
#include <Wire.h>
#include <Arduino.h>


float flow;
//-- Flow - breath detection ------------------------------------------------------
#define fThreshold 50
unsigned long expStart, inspStart, PauseStart;
unsigned long now, lastSave;
const float T[25] = {  0.000540, 	0.001591, 	0.003335, 	0.005896, 	0.009318, 	0.013540, 	0.018380, 	0.023538, 	0.028624, 	0.033202, 	0.036844, 	0.039190, 	0.040000, 	0.039190, 	0.036844, 	0.033202, 	0.028624, 	0.023538, 	0.018380, 	0.013540, 	0.009318, 	0.005896, 	0.003335, 	0.001591, 	0.00054};
float drv2nd;
float Flpm[25];
float FF[3], DRV[5];
float peak, peakNeg, peakPos;
float sp, sn;
uint8_t numBreath;
uint8_t buff;
bool countingPauseEN;
bool FlpmFull;
uint8_t breathState, stateChg;
bool inhaleNotValid, exhaleNotValid;
float volume,maxv,minv,vavgO;

//-- Turbine - Filter -------------------------------
uint8_t miniState;
uint8_t n;
float lastflow;

//-- Monitor - Respiratory Rate ---------------------
int TE,TI;
int RRarray[30];
int RRidxAdd, RRidxRem;
int RRnBreath, RRaccTime, RRTotalTime;
int RRtotal, RR;

//-- User Control Flag ------------------------------
bool RUN_FLAG, FIN_FLAG;
int MODE_FLAG;
bool FVC_acq;
unsigned long MonitorTimer, startMonitor;


//-- Flow - Spirometry ------------------------------
#define MAX_CAPACITY 4177
#define MAX_INDEX 600
float capacity;
int flow_lps;
int idxEnd1, idxEnd2;
int t, savedt;
int V[MAX_INDEX], F[MAX_INDEX];
int FVC,FEV1;
float PEF;
bool FVCManuver;
//source: https://www.actx.edu/respiratory/files/filecabinet/folder14/2305_Section9.pdf
int correctionArray[18]={1.108,1.101,1.097,1.091,1.086,1.080,1.073,1.067,1.062,1.055,1.049,1.042,1.034,1.028,1.020,1.014,1.007,1.000};
float BTPS_correction;
// save data
#include "ESPFlash.h"
 ESPFlash<int> setting("/setting");
 ESPFlash<float> resultF("/FVCresultF");
 ESPFlash<int> resultV("/FVCresultV");
 ESPFlash<int> resultPar("/FVCresultpar");

void setupRespirasiData(){

  Wire.begin();
  buff=0;
  FlpmFull=false;
  // loadAllSetting();
  setSensitivity(0.1);
  setMonitorTimer(600);
  // Serial.println(BTPS_correction);
  resetBreathCharValue();
  now=millis();
}


void ProccessData(float readings){
    now=millis();
	if(RUN_FLAG){
	  insertFlow(readings);
	  // Serial.print(breathState);Serial.print("\t");
	  // Serial.print(miniState);Serial.print("\t");
	  // Serial.print(flow);Serial.print("\t");
    // Serial.print(sp);Serial.print("\t");
    // Serial.print(sn);Serial.print("\t");
	  // Serial.print(capacity);
	  // Serial.println("");
      volumecalc();
	}
    FVC_acq = !FIN_FLAG && (MODE_FLAG==2);
	// TimerforMonitor();
	IEDetection();
}

void volumecalc(){
  if(lastflow==flow)n++;else n=0;
  if(n>=255)n=255; lastflow=flow;
  bool noChanges = (n>=30);
  // bool noChanges = (abs(flow)>fThreshold) && (DRV[0]==0 && DRV[1]==0 && DRV[2]==0 && DRV[3]==0 && DRV[4]==0);;
  switch (miniState){
    case 0:if(noChanges){if( (now-inspStart <=300 || now-expStart<=300) ){volume-=n*(flow*BTPS_correction);miniState=1;}}else{volume+=flow*BTPS_correction; }break;
    case 1:if(!noChanges){volume+=flow*BTPS_correction;miniState=0;}else {flow=0;miniState=2;}break;
    case 2:if(!noChanges){volume+=flow*BTPS_correction;miniState=0;}else {flow=0;n=0;}break;
  }
  // volume+=flow*0.1667;
  // debug();
}
void debug(){
    Serial.print(breathState*25);Serial.print("\t");
    // Serial.print(miniState*25);Serial.print("\t");
    Serial.print(flow);Serial.print("\t");
    Serial.print(sp);Serial.print("\t");
    Serial.print(sn);Serial.print("\t");
    // Serial.print(FF[0]);Serial.print("\t");
    // Serial.print(drv2nd);Serial.print("\t");
    // Serial.print(peakPos);Serial.print("\t");
    // Serial.print(peakNeg);Serial.print("\t");
    // Serial.print(capacity);Serial.print("\t");
    // Serial.print(FVC);Serial.print("\t");
    // Serial.print(" RRTotalTime: ");Serial.print(RRTotalTime);
    // Serial.println("");
}

void IEDetection(){
  /*deteksi napas, perhitungan inspirasi & ekspirasi, perhitungan simple RR:respirastion rate */
  if(countingPauseEN){
    if(abs(flow)>sp) countingPauseEN=false;
	if(now-PauseStart > 10000) {stateChg=4; countingPauseEN=false;}
  }else{
    if(abs(flow)<0.5){countingPauseEN=true;PauseStart=now;}
  }
  switch (breathState){
    case 0: {
	  if(FlpmFull) peakDetectPos();
	  // if(RUN_FLAG && (flow>sp && drv2nd>=peakPos) ){
	  if( RUN_FLAG ){
	  
	    if(flow>sp)
	      stateChg=1;
	  }
	}break;
	case 1: {
	  RRTotalTime=RRaccTime+(now-inspStart)*0.01;
	  guardRR();
	  peakDetectNeg();
	  if(FVC_acq)FVCManuverI();
	  if(volume>maxv) maxv=volume;
	  if(!RUN_FLAG){
	    stateChg=5;
	  }
	  // else if(flow<sn && drv2nd<=peakNeg){
	  else if(flow<sn /* && drv2nd<=peakNeg */){
	    stateChg=2;
	  }
	}break;
	case 2: {
	  RRTotalTime=RRaccTime+TI+(now-expStart)*0.01;
	  guardRR();
	  peakDetectPos();
	  if(FVC_acq)FVCManuverE();
	  if(volume<minv) minv=volume;
	  if(!RUN_FLAG){
	    stateChg=5;
	  }
	  // else if(flow>sp && drv2nd>=peakPos){
	  else if(flow>sp /* && drv2nd>=peakPos */){
	    stateChg=3;
	  }
	}break;
  }

  switch(stateChg){
    case 0: ;break;
	case 1:{
	  maxv=volume;
	  // if(MODE_FLAG==1) startMonitor=now;
	  inspStart=now;
	  breathState=1;Serial.print(breathState);
	}break;
    case 2:{Serial.print(breathState);
	  TI=(now-inspStart)*0.01;
	  if(TI<5) inhaleNotValid=true;
	  else{
	    inhaleNotValid=false;
		if(FVCManuver) FVCresult();
	  }
	  if(FVC_acq){
	    FVCreset();
	    FVCinit();
	  }
	  minv=volume;
	  expStart=now;
	  breathState=2;
Serial.print(breathState);
	}break;
    case 3:{
	  breathState=1;
	  TE=(now-expStart)*0.01;
	  if(TE<5) exhaleNotValid=true;
	  else exhaleNotValid=false;
	  if(!exhaleNotValid && !inhaleNotValid){
        // Serial.print(" TE: ");Serial.print(TE);
        // Serial.print(" TI: ");Serial.print(TI);
        // Serial.println("");
	    calcRR();
		if(FVC_acq){
		  isFVCmanuver();
		  if(!FVCManuver)
		    avgVOUT(maxv-minv,false);
		  Serial.print(" maxv: ");Serial.print(maxv);
		  Serial.print(" minv: ");Serial.println(minv);
		}
	  }
	  maxv=volume;
	  inspStart=now;Serial.print(breathState);
	}break;
    case 4:{
	  resetBreathCharValue();
	  // avgVOUT(0,true);
	  t=0;
	  idxEnd1=0;
	  idxEnd2=0;
	  volume=0;
	  breathState=0;Serial.print(breathState);
	}break;
    case 5:{
	  resetBreathCharValue();
	  avgVOUT(0,true);
	  t=0;
	  idxEnd1=0;
	  idxEnd2=0;
	  volume=0;
	  breathState=0;Serial.print(breathState);
	}break;
  }
  stateChg=0;
}

float filter2Hz(){
  float Fresult = 0;
  for(int x=0; x<25; x++){
    Fresult += Flpm[x]*T[x];
  }
  return Fresult*2.5;
}

void insertFlow(float readings){
  flow=readings;
  if(buff<25)
    buff++;
  else
    FlpmFull=true;
  for(int x=24; x>0; x--){
    Flpm[x] = Flpm[x-1];
  }
  Flpm[0]=flow;
}

void derivative2nd(float filter){
  FF[2]=FF[1];
  FF[1]=FF[0];
  FF[0]=filter;
  drv2nd=round((FF[2]+(-2)*FF[1]+FF[0])*10000);
  DRV[4]=DRV[3];
  DRV[3]=DRV[2];
  DRV[2]=DRV[1];
  DRV[1]=DRV[0];
  if(abs(filter)<fThreshold)
    DRV[0]= drv2nd;
  else {
    DRV[0]=0;
	drv2nd=0;
  }
}

void peakDetectPos (){
  derivative2nd(filter2Hz());
  if(DRV[2]>DRV[3] &&
     DRV[2]>DRV[4] &&
	 DRV[2]>DRV[1] &&
	 DRV[2]>DRV[0] ) peak=DRV[2];

  peakNeg=0;
  if(DRV[2]==0 || breathState==1)
    peakPos=0;
  else {
    if(peak>peakPos) peakPos=peak;
  }
}

void peakDetectNeg (){
  derivative2nd(filter2Hz());
  if(DRV[2]<DRV[3] &&
     DRV[2]<DRV[4] &&
	 DRV[2]<DRV[1] &&
	 DRV[2]<DRV[0] ) peak=DRV[2];

  peakPos=0;
  if(DRV[2]==0 || breathState==2)
    peakNeg=0;
  else {
    if(peak<peakNeg) peakNeg=peak;
  }
}
void saveToArray(){
  if(now-lastSave > 100){
    lastSave=now;
	if(t<MAX_INDEX){
      V[t]=capacity;
      F[t]=round(flow*-1.667);
	  t++;
	  // Serial.print(t);Serial.print("\t");
	  // Serial.println(breathState);
    }
  }
}
void FVCManuverE(){
    flow_lps=round(flow*-1.667);
	capacity-=flow*BTPS_correction;
    if(flow_lps > PEF)PEF=flow_lps;
    if(capacity>FVC)FVC=capacity;
    if(abs( 1000 - (now-expStart) )<20) FEV1=capacity;
	saveToArray();
}

void FVCManuverI(){
    capacity-=flow*BTPS_correction;
    flow_lps=round(flow*-1.667);
	saveToArray();
}

void FVCinit(){
  flow_lps=round(flow*-1.667);
  capacity=flow*-1*BTPS_correction;
  t=1;
  lastSave=now;

  PEF=flow_lps;
  FVC=capacity;
  FEV1=0;
}

void FVCreset(){
  FEV1=0;
  FVC=0;
  PEF=0;
  for(int x; x<t; x++){
    V[x]=0;
	F[x]=0;
  }
}

void isFVCmanuver(){
  if(numBreath>=3 && FVC>vavgO){
   idxEnd1=t;
   FVCManuver = true;
  }
  else FVCManuver = false;
   // Serial.print(" FVC: ");Serial.print(FVC);
   // Serial.print(" vavgO: ");Serial.print(vavgO);
   // Serial.print("\t");Serial.println(FVCManuver);
}

int getFVC(){
  // return FVC;
  return resultPar.getElementAt(0);
}
int getFEV1(){
  // return FEV1;
  return resultPar.getElementAt(1);
}
float getratio(){
  // return (float)FEV1/FVC;
  return (float)resultPar.getElementAt(2)/1000;
}
float getPEF(){
  // return PEF*0.01;
  return (float)resultPar.getElementAt(3)/100;
}
int getVTLength(){
  // return idxEnd1;
  return resultPar.getElementAt(4);
}
int getFVLength(){
  // return idxEnd2;
  return resultPar.getElementAt(5);
}
float getFlowArray(int idx){
  // return F[idx];
  return resultF.getElementAt(idx);
}
float getVolumeArray(int idx){
  // return V[idx];
  return resultV.getElementAt(idx);
}
void FVCresult(){
Serial.println("FVCresult");
  // if(FVCManuver){
    FIN_FLAG=true;
	idxEnd2=t;
	// FVC= 189;
	// FEV1= 157;
	// PEF= 25.00;
    // idxEnd1=54;
	// idxEnd2=102;
    Serial.print("FVC: ");Serial.print(FVC);
    Serial.print(" FEV1: ");Serial.print(FEV1);
    Serial.print(" FEV1/FVC: ");Serial.print((float)FEV1/FVC,2);
    Serial.print(" PEF: ");Serial.print(PEF);
    Serial.print(" 1st ");Serial.println(idxEnd1);
	for (int x=0; x<idxEnd1; x++){
	  Serial.print(V[x]);
	  Serial.println("");
	}
    Serial.print(" 2nd ");Serial.println(idxEnd2);
	for (int x=0; x<idxEnd2; x++){
	  Serial.print(V[x]);Serial.print("\t");Serial.print(F[x]);
	  Serial.println("");
	}
    // resultF.set(0);
    // resultV.set(0);
    resultPar.set(FVC);
    resultPar.append(FEV1);
    resultPar.append(round((1000*FEV1)/FVC));
    resultPar.append(PEF);
    resultPar.append(idxEnd1);
    resultPar.append(idxEnd2);
	float bufferF[50];
	int bufferV[50];
	int idx;
	for(int y=0; y<7; y++){
	  idx=y*50;
	  for(int x=0; x<50; x++){
	    bufferF[x]=((float)F[idx+x])/100;
		bufferV[x]=V[idx+x];
	    // bufferF[x]=F[idx]/100;
		// bufferV[x]=V[idx];
	  }
	  if(y!=0){
	    resultF.appendElements(bufferF,50);
	    resultV.appendElements(bufferV,50);
	  } else {
	    resultF.setElements(bufferF,50);
	    resultV.setElements(bufferV,50);
	  }
	}
  FVCManuver=false;
  FIN_FLAG=false;
  breathState=0;
  // }
}

// int Pidx;
// String payload;
// char out[100];

// String getSPayload(){
  // out.clear();
  // if(ID==0){
	// out["ID"] = ID++;
	// out["FVC"] = FVC;
	// out["FEV"] = FEV1;
	// out["PEF"] = PEF;
	// out["F1"] = F[Pidx]*0.01 ;
	// out["V1"] = V[Pidx]*0.001;
	// out["V2"] = V[Pidx]*0.001;
	// Pidx++;
    // serializeJson(out, payload);
  // } else if(Pidx<=idxEnd1){
	// out["ID"] = ID++;
	// out["F1"] = F[Pidx]*0.01 ;
	// out["V1"] = V[Pidx]*0.001;
	// out["V2"] = V[Pidx]*0.001;
	// Pidx++;
    // serializeJson(out, payload);
  // } else if(Pidx<idxEnd1){
	// out["ID"] = ID++;
	// out["F1"] = (float)F[Pidx]*0.01 ;
	// out["V1"] = (float)V[Pidx]*0.001;
	// Pidx++;
    // serializeJson(out, payload);
  // } else {
	// EoD_FLAG=true;
  // }
  // return payload;
// }


void avgVOUT(int vout, bool reset){
  static int vtotal;
  int newTotal;
  static int varray[5];
  static uint8_t avgidx;


  if(reset){
    newTotal=0;
	avgidx=0;
	numBreath=0;

	for(int x=0; x<5; x++){
	  varray[x]=0;
	}
  }else {
    if(vout<MAX_CAPACITY){
	  numBreath++;
      if(numBreath>5)numBreath=5;
      newTotal = vtotal - varray[avgidx] + vout;
      vtotal=newTotal;
      varray[avgidx]=vout;
      avgidx++;
      if(avgidx>=5)avgidx=0;
      vavgO=round(((float)vtotal/numBreath)*1.5);
	}
   Serial.print(" FVC: ");Serial.print(FVC);
   Serial.print(" varray: {");Serial.print(varray[0]);
   Serial.print(", ");Serial.print(varray[1]);
   Serial.print(", ");Serial.print(varray[2]);
   Serial.print(", ");Serial.print(varray[3]);
   Serial.print(", ");Serial.print(varray[4]);
   Serial.print("} n: ");Serial.print(numBreath);
   Serial.print(" vavgO: ");Serial.print(vavgO);
  }

}

void resetBreathCharValue(){
    TI=0;
	TE=0;
	RR=0;
	resetRR();
  }
// void calcRR(){
  // RR=600/(TE+TI);
// }
int getRRData(){
  // Serial.println((float)RR*0.1);
  // return (float)RR*0.1;
  return round(RR*0.1);
}
void calcRR(){
  RRnBreath++;
  RRarray[RRidxAdd] = TI+TE;
  RRaccTime += RRarray[RRidxAdd];
  RRtotal += round((float)6000/(RRarray[RRidxAdd]));
  RR=RRtotal/RRnBreath;
  // Serial.print(TI+TE);
  // Serial.print(RRtotal);
  // Serial.print(RRnBreath);
  // Serial.print(RR);
  // Serial.println((float)RR*0.1);
	  // Serial.print("{ ");
	  // for (int x=0; x<29; x++){
		// Serial.print(RRarray[x]);
		// Serial.print(", ");
	  // }
	  // Serial.print(RRarray[29]);
	  // Serial.println(" }");
	  Serial.print(" TI+TE: ");Serial.println(TI+TE);
	  Serial.print(" idxAdd: ");Serial.println(RRidxAdd);
	  Serial.print(" idxRem:");Serial.println(RRidxRem);
	  Serial.print(" addTime: ");Serial.println(RRaccTime);
	  Serial.print(" RRtotal: ");Serial.println(RRtotal);
	  Serial.print(" nNapas: ");Serial.println(RRnBreath);
      Serial.print(" RR: ");Serial.println(RR);
  RRidxAdd++;
  if(RRidxAdd>=30)
    RRidxAdd=0;
}

void guardRR(){
  if(RRTotalTime>600){
	if(RRarray[RRidxRem]!=0){
      RRnBreath--;
	  RRaccTime-=RRarray[RRidxRem];
	  RRtotal -= round((float)6000/(RRarray[RRidxRem]));
	  RRarray[RRidxRem]=0;
	  // Serial.print("{ ");
	  // for (int x=0; x<29; x++){
		// Serial.print(RRarray[x]);
		// Serial.print(", ");
	  // }
	  // Serial.print(RRarray[29]);
	  // Serial.println(" }");
	  // Serial.print(" idxAdd: ");Serial.println(RRidxAdd);
	  // Serial.print(" idxRem:");Serial.println(RRidxRem);
	  // Serial.print(" addTime: ");Serial.println(RRaccTime);
	  // Serial.print(" RRtotal: ");Serial.println(RRtotal);
	  // Serial.print(" nNapas: ");Serial.println(RRnBreath);
      // Serial.print(" RR: ");Serial.println(RR);
	  if(RRidxRem!=RRidxAdd)
	    RRidxRem++;
	  if(RRidxRem>=30)
	    RRidxRem=0;
	}
  }

}

void resetRR(){
  RRaccTime=0;
  RRtotal=0;
  RRnBreath=0;
  RRidxAdd=0;
  RRidxRem=0;
  for(int x=0; x<30; x++){
    RRarray[x]=0;
  }
  // Serial.println("RR reset");
}
float getFlowData(){
  return flow;
}
int getVolumeRTData(){
  return volume;
}
void setSensitivity(float sensitivity){
  sensitivity*=10;
  // int temporary[3]={5,60,27};
  if(sensitivity>=5 && sensitivity<=500){
    // setting.setElementAt((int)sensitivity,0);
    // setting.setElements(temporary,3);
    sp=sensitivity;
    sn=-1*sensitivity;
  }
}
void setMonitorTimer(int seconds){
  if(seconds>=1 && seconds<=32000){
    MonitorTimer=seconds*1000;
    // setting.setElementAt(seconds,1);
  }
}
void setATemperature(int AT){
  if(AT>=20 && AT<=37){
    BTPS_correction=correctionArray[AT-20]*0.01667;
    // setting.setElementAt(AT,2);
  }
}
void loadAllSetting(){
  int setting_temp;
  BTPS_correction=0.1667;
  setting_temp=setting.getElementAt(0);
  if(setting_temp>=5 && setting_temp<=500){
    sp=setting_temp/10;
    sn=-0.1*setting_temp;
  }
  setting_temp=setting.getElementAt(1);
  if(setting_temp>=1 && setting_temp<=32000)
    MonitorTimer=setting_temp;
  setting_temp=setting.getElementAt(2);

  if(setting_temp>=20 && setting_temp<=37)
    // BTPS_correction= correctionArray[setting_temp-20]*0.01667;
    BTPS_correction= 0.1667;
}
void setSTOP_FLAG (){
  RUN_FLAG=false;
  FIN_FLAG=true;
  Serial.print("RUN_FLAG: ");
  Serial.println(RUN_FLAG);
}
void setRUN_FLAG (){
  RUN_FLAG=true;
  FIN_FLAG=false;
  Serial.print("RUN_FLAG: ");
  Serial.println(RUN_FLAG);
}
// bool getFIN_FLAG(bool FIN_FLAG){
  // return FIN_FLAG;
// }

// void resetFIN_FLAG(){
  // FIN_FLAG=false;
// }

void setMODE_FLAG(int mode_en){
  MODE_FLAG=mode_en;
  Serial.print("MODE: ");
  Serial.println(mode_en);
}

// void TimerforMonitor(){
  // if(RUN_FLAG && MODE_FLAG==1){
    // if(now-startMonitor >= MonitorTimer){
	  // FIN_FLAG=true;
	  // RUN_FLAG=false;
	// }
  // }
// }
/*
void setupRespirasiData(){

  Wire.begin();
  error = NO_ERROR;
  error = SF05_SoftReset();
  error = SF05_GetFlow(OFFSET_FLOW, SCALE_FLOW, &flow_s);
  delay(1000);
  error = SF05_GetFlow(OFFSET_FLOW, SCALE_FLOW, &flow_s);

  resetBreathCharValue();
  lastMsg=millis()+100;
  lastGetFlow=lastMsg-20;
  lastProccessData=lastMsg-30;
}


void readFlowSensor(){
  if (now - lastGetFlow >= 10){
	lastGetFlow=now;
    error = SF05_GetFlow(OFFSET_FLOW, SCALE_FLOW, &flow_s);
	flmv=flowmv(&flow,totalF,arrayF);
  }
}

void ProccessData(){
  if (now - lastProccessData >=100){
    lastProccessData=now;
	flow=flmv;
    int Lin = round(flow*10/6);
	int Lout= round(flow*-10/6);
	volume+=Lin;
    if(breathState==4)volumeOut+=Lout;
	if(breathState==2)volumeIn +=Lin;
	if(breathState==4 && flow<PEF)PEF=flow;
	if(breathState>0){
	  VT[n]=volume;
	  n++;
	}
	if(breathState>=7){
	  volumeOut+=Lout
	  F[t]=flow*-100;
	  V[t]=volumeOut;
	  t++;
	  if(volumeOut>VOUT)VOUT=volumeOut;
	  if(flow<PEF)PEF=flow;
	  if(abs(1000-(now-expStart))<2)FEV1=volumeOut;
	}
  }
}
void IEDetectionSP(float sensitivity){
  switch(breathState){
    case 0:
	  if(flow>1){
	    inspStart=now;
		breathState=2;
		breathChg=2;
		n=1;
		resetVT();
	  }break; //idle
    case 1:
	  if(now-ePauseStart>120000){
	    resetBreathCharValue();
	    breathState=0;
	    breathChg=5;
	  }
	  else if(flow>5){
	    RR=60/(TE+TI);
		TE=(now-expStart)*0.001;
		inspStart=now;
		breathState=2;
		breathChg=2;
	  }break; //pause exhale
	case 2:
	  if((volumeIn>viavg+300) && numBreath>1){
	    breathState=5;
		breathChg=6;
	  }
	  else if(abs(0-flow)<0.1){
	    iPauseStart=now;
		breathState=3;
		breathChg=3;
	  }
	  else if(flow<-5){
	    TI=(now-inspStart)*0.001;
		viavg=volumemv(VIN, arrayVi);
		expStart=now;
		breathState=4;
		breathChg=4;
	  }break; //inhale
	case 3:
	  if(now-iPauseStart>120000){
	    resetBreathCharValue();
		breathState=0;
		breathChg=5;
	  }
	  else if(flow<-5){
	    TI=(now-inspStart)*0.001;
		viavg=volumemv(VIN, arrayVi);
		expStart=now;
		breathState=4;
		breathChg=4;
	  }break;  //pause inhale
	case 4:
	  if(abs(0-flow)<0.1){
	    ePauseStart=now;
		breathState=1;
		breathChg=1;
	  }
	  else if(flow>5){
	    RR=60/(TE+TI);
		TE=(now-expStart)*0.001;
		inspStart=now;
		breathState=2;
		breathChg=2;
	  }break; //exhale
    case 5:
	  if(abs(0-flow)<0.1){
	    iPauseStart=now;
		breathState=6;
		breathChg=7;
	  }
	  else if(flow<-7.5){
	    TI=(now-inspStart)*0.001;
		expStart=now;
		breathState=7;
		breathChg=8;
	  }break; //inhale
    case 6:
	  if(now-iPauseStart>120000){
	    resetBreathCharValue();
		breathState=0;
		breathChg=5;
	  }
	  else if(flow<-7.5){
	    TI=(now-inspStart)*0.001;
		expStart=now;
		breathState=7;
		breathChg=8;
	  }break;
    case 7:
	  if(abs(0-flow)<0.1 && (now-inspStart>=200)){
	    ePauseStart=now;
		breathState=8;
		breathChg=9;
	  }
	  else if(flow>5&& (now-inspStart>=200)){
	    RR=60/(TE+TI);
		TE=(now-expStart)*0.001;
		inspStart=now;
		breathState=9;
		breathChg=10;
	  }break;
	case 8:
	  if(now-ePauseStart>120000){
	    resetBreathCharValue();
		breathState=0;
		breathChg=5;
	  }
	  else if(flow>5){
	    RR=60/(TE+TI);
		TE=(now-expStart)*0.001;
		inspStart=now;
		breathState=9;
		breathChg=10;
	  }break;
	case 9:
	  if(abs(0-flow)<0.1){
	    iPauseStart=now;
		breathState=3;
		breathChg=3;
	  }
	  else if(flow<-5){
	    TI=(now-inspStart)*0.001;
		expStart=now;
		breathState=4;
		breathChg=4;
	  }break;
  }
  switch(breathChg){
    case 0: ;break;
	case 1: {VOUT=volumeOut;}break;
	case 2: {VOUT=volumeOut;volumeIn=flow*10/6;numBreath++;}break;
	case 3: {VIN=volumeIn;PEF=0;}break;
	case 4: {VIN=volumeIn;volumeOut=Lout;PEF=flow;FVC=0;FEV1=0;}break;
	case 5: {VIN=0;VOUT=0;volume=0;volumeIn=0;volumeOut=0; PEF=0; FVC=0;FEV1=0;numBreath=0;resetmv(arrayVi);}
	case 6: {resetFV();numBreath--;}break;
	case 7: {VIN=volumeIn;PEF=0;}break;
	case 8: {VIN=volumeIn;volumeOut=Lout;PEF=flow;FVC=0;FEV1=0;F[1]=flow*-100;V[1]=volumeOut;t=2;}break;
	case 9: {VOUT=volumeOut;FVC=VOUT;}break;
	case 10:{VOUT=volumeOut;FVC=VOUT;volumeIn=flow*10/6;}break;
  }

}
  switch(breathState){
    case 0:
	  if(flow>sp){
	    inspStart=now;
		breathState=2;
	  }break; //idle
    case 1:
	  if(now-ePauseStart>120000){
		breathState=0;
	  }else if(flow>sp){
		inspStart=now;
		breathState=2;
	  }break; //pause exhale
	case 2:
	  if(abs(0-flow)<0.5){
	    iPauseStart=now;
		breathState=3;
	  }else if(flow<sn){
		expStart=now;
		breathState=4;
	  }break; //inhale
	case 3:
	  if(now-iPauseStart>120000){
		breathState=0;
	  } else if(flow<sn){
		TI=(now-inspStart)*0.001;
		expStart=now;
		breathState=4;
	  }break;  //pause inhale
	case 4:
	  if(abs(0-flow)<0.5){
	    ePauseStart=now;
	    breathState=1;
	  }else if(flow>sp){
	    inspStart=now;
	    breathState=2;
	  }break; //exhale
  }
  */
