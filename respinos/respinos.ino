#include <ArduinoJson.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WebServer.h>
#include "src/breathAnalysis/breathAnalysis.h"
#include "src/libraries/PubSubClient/PubSubClient.h"
#include "src/libraries/TaskScheduler/TaskScheduler.h"

#define LED_PIN 2
#define pwrLED1 13
#define pwrLED0 2
#define modeLED1 0
#define modeLED0 16
#define pairPB0 15
#define LED_MODE0_ON digitalWrite(modeLED0,LOW);
#define LED_MODE0_OFF digitalWrite(modeLED0,HIGH);
#define LED_MODE1_ON digitalWrite(modeLED1,LOW);
#define LED_MODE1_OFF digitalWrite(modeLED1,HIGH);
#define LED_PWR0_ON digitalWrite(pwrLED0,LOW);
#define LED_PWR0_OFF digitalWrite(pwrLED0,HIGH);
#define LED_PWR1_ON digitalWrite(pwrLED1,LOW);
#define LED_PWR1_OFF digitalWrite(pwrLED1,HIGH);
#define LOWBAT_TRESHOLD 874

#define MAX_SP02_VALUE 100
#define MIN_SP02_VALUE 50
#define MAX_TEMP_VALUE 4200
#define MIN_TEMP_VALUE 3200

#define WIFI_CONNECT_TIMEOUT 10  // in second
#define MQTT_CONNECT_TIMEOUT 10  // in second

#define SEND_DATA_INTERVAL 2

#define RESET_USER_URL "http://202.148.1.57:8501/api/v1/device/reset"
#define REMOTE_SERVER_HOST "202.148.1.57"
#define REMOTE_SERVER_PORT 8501

/******************* GLOBAL VARIABLE *********************/

// WiFi parameters
const char *ssidAP = "respinos04";
const char *passwordAP = "12345678";
bool networkTimeout = false;
bool networkConnected = false;
bool mqttTimeout = false;
bool mqttConnected = false;

// Measurement parameters
#include <Wire.h>
union intToBytes {
    char buffer[2];
    int16_t intReading;
  
} iconverter;
union floatToBytes {
    char buffer[4];
    float flowReading;
  
} converter;
char i2cbuffer[4]; 
uint8_t respiratoryRate = 0;
int16_t bodyTemperature;
int16_t spo2;
int batteryLvl;

StaticJsonDocument<128> doc;
IPAddress myIP(0, 0, 0, 0);

// State enum
enum RespinosStateEnum {
  RESPINOS_STATE_ERROR = -1,
  RESPINOS_STATE_DISCONNECTED,
  RESPINOS_STATE_READY,
  RESPINOS_STATE_RUNNING
};

RespinosStateEnum systemState = RESPINOS_STATE_DISCONNECTED;
bool RESPINOS_LOW_BATTERY =false;
/*********************************************************/

/************************ MQTT ***************************/

// Broker parameters
char *mqtt_broker = "202.148.1.57";
char *mqtt_username = "respinoscl";
char *mqtt_password = "X5l4SsKJkXFy253s3X2ISJvqeU8FFi";
int mqtt_port = 1889;
char *deviceId = "respinos04";

char *dataTopic = "respinos04";
char *cmdTopic = "respinos04/cmd";
char *stateTopic = "respinos04/state";

IPAddress mqttServer(202, 148, 1, 57);

// MQTT callback, executed when receive subscribed data
void mqttCallback(char *topic, byte *payload, unsigned int length);

WiFiClient espClient;
WiFiClient cli;
HTTPClient http;
PubSubClient client(mqttServer, mqtt_port, mqttCallback, espClient);

/*********************************************************/

/******************** TASK SCHEDULER *********************/

Scheduler userScheduler;

void sendData();
void getMeasurementData();
void LEDblink(int LED);
bool detectLongPush(int PB);
// Task to send data every 2 seconds
Task taskSendData(TASK_SECOND * SEND_DATA_INTERVAL, TASK_FOREVER, &sendData);

Task taskChangeState(TASK_SECOND * 10, TASK_FOREVER, []() {
  if (systemState == RESPINOS_STATE_READY) {
    client.publish("respinos01/cmd", "{\"cmd\":\"start\",\"mode\":\"basic\"}");
  } else {
    client.publish("respinos01/cmd", "{\"cmd\":\"stop\",\"mode\":\"basic\"}");
  }

  client.publish("respinos01/cmd", "{\"cmd\":\"state\",\"mode\":\"basic\"}");
});

bool onFlag = false;
Task taskBlinkLed(TASK_SECOND, TASK_FOREVER, []() {
  if (onFlag) {
    onFlag = false;
    digitalWrite(pwrLED1, LOW);
  } else {
    onFlag = true;
    digitalWrite(pwrLED1, HIGH);
  }
  // LEDblink(modeLED1);
});

Task taskProcessFlow( 10, TASK_FOREVER, []() {
  Wire.requestFrom(8, 5);
  uint8_t index = 0;
  char tipe=0;
  uint8_t flushw;
  while (Wire.available()){
    if(tipe==0){
	  tipe=Wire.read();
	} else if(tipe=='f'){
      converter.buffer[index] = Wire.read();
      index++;
	} else flushw=Wire.read();
  }
  if(tipe=='f')ProccessData(converter.flowReading);
});

Task taskBatteryCheck(TASK_SECOND, TASK_FOREVER, []() {
  batteryLvl = analogRead(A0);
  if(!RESPINOS_LOW_BATTERY){
    if(batteryLvl<LOWBAT_TRESHOLD) {
	  RESPINOS_LOW_BATTERY = true;
	  taskBlinkLed.enable();
	  LED_PWR0_OFF;
	}
  } else {
    if(batteryLvl>LOWBAT_TRESHOLD+86){
	  RESPINOS_LOW_BATTERY = false;
	  taskBlinkLed.disable();
	  LED_PWR0_ON;
	  LED_PWR1_OFF;
	  onFlag = false;
	}
  }
  Wire.requestFrom(8, 5);
  uint8_t index = 0;
  char tipe=0;
  uint8_t flushw;
  while (Wire.available()){
    if(tipe==0){
	  tipe=Wire.read();
	} else if(tipe=='d'){
      i2cbuffer[index]=Wire.read();
	  index++;
	} else flushw=Wire.read();
  }
  Serial.print("batterylvl: ");Serial.println(batteryLvl);
});

Task taskDebug(TASK_SECOND * 2, TASK_FOREVER, []() {
  getMeasurementData();

  doc.clear();

  JsonObject payload = doc.createNestedObject("payload");
  payload["RR"] = respiratoryRate;
  payload["SPO2"] = spo2;
  payload["T"] =bodyTemperature*0.01;

  String msg;
  serializeJson(doc, msg);
  Serial.print(converter.flowReading);
  Serial.print(' ');
  Serial.println(msg.c_str());
});

/*********************************************************/

/***************** FUNCTION DECLARATION ******************/

void startMeasurement();
void stopMeasurement();

/*********************************************************/

/******************* SERVER DECLARATION ******************/

ESP8266WebServer server(80);  // Initialize the server on Port 80
void handleLED();
void handleRoot();
void handleSetWifi();
void handleResetUser();

/*********************************************************/

void setup(void) {
  uint8_t timeoutCount = 0;					   
  // Start Serial
  Serial.begin(115200);
  setupRespirasiData();
  
  pinMode(pairPB0, INPUT);
  pinMode(modeLED0, OUTPUT);
  pinMode(modeLED1, OUTPUT);
  pinMode(pwrLED0, OUTPUT);
  pinMode(pwrLED1, OUTPUT);
  LED_PWR0_ON;
  LED_PWR1_OFF;
  LED_MODE0_OFF;
  LED_MODE1_OFF;
  
  // Connect to WiFi
  // WiFi.begin(ssid, password);
  // WiFi.begin("Smart Xirka","engineer@30");
  // Try to connect to last ssid
  // Uncomment to forget ssid on start up
  // WiFi.disconnect(true);
  // WiFi.persistent(false);

  // Set wifi mode to AP & STA
  WiFi.mode(WIFI_AP_STA);

  // Define ssid & password
  WiFi.softAP(ssidAP, passwordAP);
  while (!networkTimeout && WiFi.status() != WL_CONNECTED) {
    delay(500);
	LEDblink(modeLED0);
	delay(500);
	LEDblink(modeLED0);
    Serial.print(".");
    timeoutCount++;
    if (timeoutCount >= WIFI_CONNECT_TIMEOUT) {
      networkTimeout = true;
      Serial.println("\nWifi Timeout");
    }
  }
  
  // Add tasks to the scheduler
  userScheduler.addTask(taskProcessFlow);
  userScheduler.addTask(taskBatteryCheck);
  userScheduler.addTask(taskChangeState);
  userScheduler.addTask(taskBlinkLed);
  userScheduler.addTask(taskDebug);
  userScheduler.addTask(taskSendData);
  taskChangeState.enable();
  taskProcessFlow.enable();
  taskBatteryCheck.enable();
  taskDebug.enable();
  userScheduler.startNow();
  
  // Set webserver
  server.on("/", handleRoot);
  server.on("/LED", HTTP_POST, handleLED);
  server.on("/set-wifi", HTTP_POST, handleSetWifi);
  // server.on("/reset-user", HTTP_POST, handleResetUser);
  server.onNotFound(handleNotFound);
  server.begin();  // Start the HTTP Server

  IPAddress HTTPS_ServerIP = WiFi.softAPIP();
  Serial.print("Server IP is: ");
  Serial.println(HTTPS_ServerIP);
  
  // Set system state
  if (!networkTimeout) {  // Connected to the network
    networkConnected = true;
    timeoutCount = 0;

    Serial.println("");
    Serial.println("WiFi connected");

    client.setServer(mqtt_broker, mqtt_port);
    while (!client.connected() && !mqttTimeout) {
      String client_id = "respinos-";
      client_id += String(WiFi.macAddress());
      Serial.printf("The client %s connects to the xirka iot mqtt broker\n",
                    client_id.c_str());
      if (client.connect(client_id.c_str(), mqtt_username, mqtt_password)) {
        Serial.println("xirka iot mqtt broker connected");
      } else {
        Serial.print("failed with state ");
        Serial.print(client.state());
        delay(2000);
      }

      timeoutCount++;
      if (timeoutCount >= MQTT_CONNECT_TIMEOUT) {
        mqttTimeout = true;
      }
    }

    if (!mqttTimeout) {
      mqttConnected = true;
      Serial.print("topic subs: ");
      Serial.println(cmdTopic);
      client.subscribe(cmdTopic);
      systemState = RESPINOS_STATE_READY;
	  LED_MODE0_ON;
    }
  } else {
    Serial.println("RESPINOS_STATE_DISCONNECTED");
    systemState = RESPINOS_STATE_DISCONNECTED;
	LED_MODE0_OFF;
  }
  
  setMODE_FLAG(1);
  setRUN_FLAG();
}

void loop() {
  switch (systemState) {
    case RESPINOS_STATE_DISCONNECTED:
      server.handleClient();
      break;
    default: {
      userScheduler.execute();
      client.loop();
      server.handleClient();
	  if(detectLongPush(pairPB0)){
	    handleResetUser();
	  }
    }
  }
}

void handleRoot() {
  doc.clear();

  doc["network"] = networkConnected ? "Connected" : "Not Connected";
  doc["mqtt"] = mqttConnected ? "Connected" : "Not Connected";

  String msg;
  serializeJson(doc, msg);
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200, "text/json", msg.c_str());
}
void connectWiFi(uint16_t timeout){
  unsigned long tm=millis();
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
	LEDblink(modeLED0);
	if(millis()-tm > timeout)
      break;
  }
}
void handleSetWifi() {
  networkTimeout = false;
  Serial.println("handleSetWifi");
  uint8_t timeoutCount = 0;

  DeserializationError error = deserializeJson(doc, server.arg("plain"));
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.f_str());
    server.sendHeader("Access-Control-Allow-Origin", "*");
    server.send(400, "text/json", error.f_str());
    return;
  }

  const char *ss = doc["ssid"];
  const char *pw = doc["password"];

  doc.clear();
  doc["message"] = "ok";

  String msg;
  serializeJson(doc, msg);
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200, "text/json", msg.c_str());

  Serial.print("ssid: ");
  Serial.println(ss);
  Serial.print("passwd: ");
  Serial.println(pw);

  // Connect to WiFi
  WiFi.begin(ss, pw);
  connectWiFi(WIFI_CONNECT_TIMEOUT * 1000);
  // if (WiFi.waitForConnectResult(WIFI_CONNECT_TIMEOUT * 1000) != WL_CONNECTED) {
  if (WiFi.status() != WL_CONNECTED) {
    Serial.println("Connection Timeout");
    Serial.print("status: ");
    Serial.println(WiFi.status());
    networkConnected = false;
    WiFi.reconnect();
	LED_MODE0_OFF;
  } else {
    networkConnected = true;

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.print("SSID: ");
    Serial.println(WiFi.SSID());

    // Print the IP address
    Serial.print("Local IP: ");
    Serial.println(WiFi.localIP());

    // Connect to MQTT server
    timeoutCount = 0;
    mqttTimeout = false;

    Serial.println("");
    Serial.println("WiFi connected");

    client.setServer(mqtt_broker, mqtt_port);
    while (!client.connected() && !mqttTimeout) {
      String client_id = "respinos-";
      client_id += String(WiFi.macAddress());
      Serial.printf("The client %s connects to the xirka iot mqtt broker\n",
                    client_id.c_str());
      if (client.connect(client_id.c_str(), mqtt_username, mqtt_password)) {
        Serial.println("xirka iot mqtt broker connected");
      } else {
        Serial.print("failed with state ");
        Serial.print(client.state());
        delay(2000);
      }

      timeoutCount++;
      if (timeoutCount >= MQTT_CONNECT_TIMEOUT) {
        mqttTimeout = true;
      }
    }

    if (!mqttTimeout) {
      mqttConnected = true;
      Serial.print("topic subs: ");
      Serial.println(cmdTopic);
      client.subscribe(cmdTopic);
      doc.clear();
      doc["state"] = systemState == RESPINOS_STATE_RUNNING ? "running" : "idle";

      String msg;
      serializeJson(doc, msg);

      Serial.printf("publish [%s] : %s\n", stateTopic, msg.c_str());
      client.publish(stateTopic, msg.c_str());
      systemState = RESPINOS_STATE_READY;
	  LED_MODE0_ON;
    }
  }
  
    Serial.print("end handleSetWiFi status: ");
    Serial.println(WiFi.status());
}

void handleLED() {
  digitalWrite(LED_PIN, !digitalRead(LED_PIN));
  server.sendHeader("Location", "/");
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(303);
}

void handleResetUser() {
  if (systemState == RESPINOS_STATE_RUNNING) {
    return;
  }
  doc.clear();
  doc["_id"] = ssidAP;
  doc["token"] = passwordAP;
  doc["secret"] = mqtt_password;

  String data;
  serializeJson(doc, data);
  Serial.println("Push to server");
  Serial.println(data.c_str());

  http.begin(cli, RESET_USER_URL);
  http.addHeader("Content-Type", "application/json");
  int httpResponseCode = http.POST(data.c_str());

  Serial.print("HTTP Response code: ");
  Serial.println(httpResponseCode);
  // Free resources
  http.end();

  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200);
}

void handleNotFound() {
  if (server.method() == HTTP_OPTIONS) {
    server.sendHeader("Access-Control-Allow-Origin", "*");
    server.sendHeader("Access-Control-Max-Age", "10000");
    server.sendHeader("Access-Control-Allow-Methods", "PUT,POST,GET,OPTIONS");
    server.sendHeader("Access-Control-Allow-Headers", "*");
    server.send(204);
  } else {
    server.send(404, "text/plain", "");
  }
}
void getMeasurementData() {
  
  int16_t temp;
	
  iconverter.buffer[0]=i2cbuffer[0];
  iconverter.buffer[1]=i2cbuffer[1];
  temp=iconverter.intReading;
  Serial.print("spo2 ");Serial.print(temp);
  if(temp!=0) {
    if(temp<MIN_SP02_VALUE || temp>MAX_SP02_VALUE) spo2=0;
	else 
	spo2=temp;
  }	
  iconverter.buffer[0]=i2cbuffer[2];
  iconverter.buffer[1]=i2cbuffer[3];
  temp=iconverter.intReading;
  Serial.print(" T ");Serial.print(temp);
  if(temp!=0){
	if(temp<MIN_TEMP_VALUE || temp>MAX_TEMP_VALUE) bodyTemperature=0;
	else 
	bodyTemperature=temp;
  }	
  
  respiratoryRate = getRRData();
}

void sendData() {
  getMeasurementData();

  doc.clear();

  JsonObject payload = doc.createNestedObject("payload");
  payload["RR"] = respiratoryRate;
  payload["SPO2"] = spo2;
  payload["T"] = bodyTemperature*0.01;

  String msg;
  serializeJson(doc, msg);

  Serial.println(msg.c_str());

  client.publish(dataTopic, msg.c_str());
}

void mqttCallback(char *topic, byte *payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  char *s = (char *)payload;
  doc.clear();

  DeserializationError error = deserializeJson(doc, s);
  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    // Serial.println(error.f_str());
    return;
  }

  const char *cmd = doc["cmd"];
  const char *mode = doc["mode"];

  Serial.print("cmd: ");
  Serial.println(cmd);
  Serial.print("mode: ");
  Serial.println(mode);

  if (!strcmp(cmd, "start")) {
    startMeasurement();
  } else if (!strcmp(cmd, "stop")) {
    stopMeasurement();
  } else if (!strcmp(cmd, "state")) {
    doc.clear();
    doc["state"] = systemState == RESPINOS_STATE_RUNNING ? "running" : "idle";

    String msg;
    serializeJson(doc, msg);

    Serial.printf("publish [%s] : %s\n", stateTopic, msg.c_str());
    client.publish(stateTopic, msg.c_str());
  }
}

void startMeasurement() {
  Serial.println("startMeasurement");
  if (systemState == RESPINOS_STATE_READY) {
    taskSendData.enable();
	LED_MODE1_ON;LED_MODE0_OFF;
    systemState = RESPINOS_STATE_RUNNING;
  }
}

void stopMeasurement() {
  Serial.println("stopMeasurement");
  if (systemState == RESPINOS_STATE_RUNNING) {
    taskSendData.disable();
	LED_MODE1_OFF;LED_MODE0_ON;
    systemState = RESPINOS_STATE_READY;
  }
}

//blink. tiap dipanggil state led toggling
void LEDblink(int LED){
  
  uint8_t LEDstate;
  LEDstate=digitalRead(LED);
  if (LEDstate) {
    digitalWrite(LED, LOW); //led nyala
  } else {
    digitalWrite(LED, HIGH); //led mati
  }
}
bool detectLongPush(int PB){
  static unsigned long chstart;
  static uint8_t step;
  uint8_t PBstate;
  PBstate=digitalRead(PB);
  switch (step){
    case 0: if(PBstate==HIGH) {Serial.println("step=1");step=1; chstart=millis();} break;
    case 1: if(PBstate==LOW && (millis()-chstart <1200)){Serial.println("step=0");step=0;} else if( millis()-chstart >=1200 ){Serial.println("step=2");step=2;LEDblink(modeLED0); delay(50);LEDblink(modeLED0);}break;
    case 2: if(PBstate==LOW){step=3;Serial.println("step=3");} break;
    case 3: {step=0;Serial.println("step=0");} break;
  }
  if(step!=3)
    return false;
  else
    return true;
}