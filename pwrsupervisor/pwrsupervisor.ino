#include <avr/sleep.h>

//PD2/INT0/PCINT18/physical 32
#define ONOFF_BUTTON 2
//28/ADC5/PC5
#define batADC_pin A5
//PD3,1
#define BOOST_EN 3
//PD4,2
#define PWR_GATE_EN 4

bool mcuON;
bool cmdsleep;
unsigned long timeNow;
unsigned long lastcheckbat;
int t;
int t1;
uint8_t step;

void setup(){
  Serial.begin(57600);
  Serial.println("start");
  
  analogReference(INTERNAL);
  pinMode(batADC_pin,INPUT);
  pinMode(BOOST_EN,OUTPUT);
  pinMode(PWR_GATE_EN,OUTPUT);
  digitalWrite(BOOST_EN,HIGH);
  digitalWrite(PWR_GATE_EN,LOW);
  pinMode(ONOFF_BUTTON,INPUT);
  
  mcuON=false;
  t=0;
  // Serial.println("1");
  cmdsleep=false;
}

void loop(){
   
  //just after uc on
   if(mcuON){
    // Serial.println("2");
    while(!digitalRead(ONOFF_BUTTON)){
	  if(t>=100 && !verylowbat()) pwronsequence();
      t++;
      delay(10);
    }
	
    mcuON=false;
    cmdsleep=false;
    if(t<100){
      // Serial.println("3");
      sleepsequence();
    } else {
      Serial.println(analogRead(batADC_pin));
      if(verylowbat()){
        pwronsequence();
        delay(1500);
        // Serial.println("5");
        sleepsequence();
      }
    }
  } else {
    timeNow=millis();
	if(timeNow - lastcheckbat >=500){
	  lastcheckbat = timeNow;
	  if( verylowbat() ) sleepsequence();
	}
    if(detectLongPush(ONOFF_BUTTON))
      cmdsleep=true;
  }
  if(cmdsleep && !mcuON)
  sleepsequence();
}

bool detectLongPush(int PB){
  static unsigned long chstart;
  uint8_t PBstate;
  PBstate=digitalRead(PB);
  switch (step){
    case 0: if(PBstate==LOW) {step=1; t1=0;} break;
    case 1: if(PBstate==HIGH && (t1<120)){step=0;} 
	  else if(t1>=120 ){
	    step=2;
	  } else {t1++;delay(10);}
	break;
    case 2: {step=0;} break;
  }
  if(step!=2)
    return false;
  else
    return true;
}

void intRoutine(){
  detachInterrupt(digitalPinToInterrupt(2));
  t=0;
  mcuON=true;
}

void sleepsequence(){
  // digitalWrite(13,LOW);
  digitalWrite(PWR_GATE_EN,HIGH);
  digitalWrite(BOOST_EN,LOW);
  while(!digitalRead(ONOFF_BUTTON)){;}
  cli(); // deactivate interrupts
  attachInterrupt(digitalPinToInterrupt(2), intRoutine, FALLING);
  // cli(); // deactivate interrupts
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable(); // sets the SE (sleep enable) bit
  sei(); //
  sleep_cpu(); // sleep now!!
  sleep_disable(); // deletes the SE bit
  detachInterrupt(digitalPinToInterrupt(2));
}

void pwronsequence(){
  digitalWrite(PWR_GATE_EN,LOW);
  digitalWrite(BOOST_EN,HIGH);
}

bool verylowbat(){
  Serial.println(analogRead(batADC_pin));
  if(analogRead(batADC_pin)<=768){
    return true;
  } else return false;
}
